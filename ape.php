<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TUGAS OOP PKS DIGITAL SCHOOL</title>

</head>
<body>
    <?php
        require_once 'animal.php';

        class Ape extends Animal{
            public $legs = 2;
            public $yell;

            public function set_yell($yell){
                $this -> yell = $yell;

            }

            public function get_yell(){
                return $this -> yell;
            }


        }

    ?>
</body>