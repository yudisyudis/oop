<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TUGAS OOP PKS DIGITAL SCHOOL</title>

</head>
<body>
    <?php

        require_once 'animal.php';
        require_once 'frog.php';
        require_once 'ape.php';

        $sheep = new Animal("shaun");
        $kodok = new Frog("buduk");
        $monyet = new Ape("kera sakti");

        $kodok -> set_jump('hop hop');
        $monyet -> set_yell('Auuoooo');


        echo 'Name: '.$sheep -> get_name(). '<br>';
        echo 'Legs: '.$sheep -> get_legs(). '<br>';
        echo 'Cold blooded: '.$sheep -> get_cold(). '<br>';
        
        echo '<br>';
        echo '<br>';

        echo 'Name: '.$kodok -> get_name(). '<br>';
        echo 'Legs: '.$kodok -> get_legs(). '<br>';
        echo 'Cold blooded: '.$kodok -> get_cold(). '<br>';
        echo 'Jump: '.$kodok -> get_jump(). '<br>';

        echo '<br>';
        echo '<br>';

        echo 'Name: '.$monyet -> get_name(). '<br>';
        echo 'Legs: '.$monyet -> get_legs(). '<br>';
        echo 'Cold blooded: '.$monyet -> get_cold(). '<br>';
        echo 'Yell: '.$monyet -> get_yell(). '<br>';


        
    ?>
</body>