<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TUGAS OOP PKS DIGITAL SCHOOL</title>

</head>
<body>
    <?php

        class Animal{

            public $legs = 4;
            public $cold_blooded = 'no';

            public function __construct($name){
                $this -> name = $name;

            }

            public function get_name(){
                return $this -> name;
            }

            public function get_legs(){
                return $this -> legs;
            }

            public function get_cold(){
                return $this -> cold_blooded;
            }

        }

    ?>
</body>